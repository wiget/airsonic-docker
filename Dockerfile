FROM adoptopenjdk/openjdk8:jdk8u242-b08-debianslim-slim

LABEL description="Airsonic is a free, web-based media streamer, providing ubiquitious access to your music." \
      url="https://github.com/airsonic/airsonic"

ENV AIRSONIC_VERSION=10.6.2 \
    AIRSONIC_PORT=4040 \
    AIRSONIC_DIR=/airsonic \
    CONTEXT_PATH=/

WORKDIR $AIRSONIC_DIR

RUN apt-get update \
    && apt-get install -y \
       bash \
       ca-certificates \
       curl \
       ffmpeg \
       fontconfig \
       lame \
       openssl \
       ttf-dejavu \
       wget \
    && apt clean

COPY run.sh /usr/local/bin/run.sh

RUN chmod +x /usr/local/bin/run.sh

RUN wget --progress=dot:giga -O airsonic.war "https://github.com/airsonic/airsonic/releases/download/v${AIRSONIC_VERSION}/airsonic.war"

EXPOSE $AIRSONIC_PORT

VOLUME $AIRSONIC_DIR/data $AIRSONIC_DIR/music $AIRSONIC_DIR/playlists $AIRSONIC_DIR/podcasts

HEALTHCHECK --interval=15s --timeout=3s CMD wget -q "http://localhost:${AIRSONIC_PORT}${CONTEXT_PATH}rest/ping" -O /dev/null || exit 1

ENTRYPOINT ["run.sh"]

